﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Sortia.Models;

namespace Sortia.Helpers
{
    public class Dictionaries
    {
        private static SortiaEntities db = new SortiaEntities();

        public static string CheckDict(string dictName)
        {
            SortiaEntities db = new SortiaEntities();
            bool checkdict;
            try
            {
                 checkdict = db.Database.SqlQuery<string>(@"SELECT name FROM sys.tables where name = @p0", dictName).Any();
            }
            catch
            {
                return "Unexpected problems with connecting to database.";
            }
            if (checkdict==false)
            {
                return "Bad Request! Given dictionary doesnt exist.";
            }
            return checkdict.ToString();
        }

        public static object GetDict(string dictName)
        {
            object dictcontainer = db.Database.SqlQuery<dictRow>(@"exec getDict @p0", dictName);
            return dictcontainer;
        }
        private class dictRow
        {
            public int ID { get; set; }
            public string Value { get; set; }
        }
    }
}