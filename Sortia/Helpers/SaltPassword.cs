﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using System.Web.Configuration;

namespace Sortia.Helpers
{
   
    public class SaltPassword
    {

        private static string Get_Salt ()
        {
            
            try
            {
                string codedsalt = WebConfigurationManager.AppSettings["Salt"];
                string salt = Decode(codedsalt);
                return salt;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in Getting Salt" + ex.Message);
            }
        }

       
        private static string Decode(string sData)    
        {
            try
            {
                var encoder = new UTF8Encoding();
                Decoder utf8Decode = encoder.GetDecoder();
                byte[] todecodeByte = Convert.FromBase64String(sData);
                int charCount = utf8Decode.GetCharCount(todecodeByte, 0, todecodeByte.Length);
                char[] decodedChar = new char[charCount];
                utf8Decode.GetChars(todecodeByte, 0, todecodeByte.Length, decodedChar, 0);
                string result = new String(decodedChar);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("Error in Decode" + ex.Message);
            }
        }

        public static string getSalted (string password)
        {
            MD5 md5Hash = MD5.Create();
            string md5pass = GetMd5Hash(md5Hash, password);
            string salt = Get_Salt();
            byte[] bytes = Encoding.UTF8.GetBytes(md5pass + salt);
            SHA256Managed sha256hashstring = new SHA256Managed();
            byte[] hash = sha256hashstring.ComputeHash(bytes);
            return ByteArrayToString(hash);
        }

        private static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        private static string GetMd5Hash(MD5 md5Hash, string input)
        {
            try
            {
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
                StringBuilder sBuilder = new StringBuilder();
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }
                return sBuilder.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("Error in Md5Hash" + ex.Message);
            }
        }

    }
   
}

