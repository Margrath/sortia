﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Sortia.Models;

namespace Sortia.Controllers
{
    public class MantisesController : ApiController
    {
        private SortiaEntities db = new SortiaEntities();

        // GET: api/Mantises
        public IQueryable<Mantises> GetMantises()
        {
            return db.Mantises;
        }

        // POST: api/Mantises
        [ResponseType(typeof(Mantises))]
        public IHttpActionResult PostMantises(Mantises mantises)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            mantises.DateCreated = DateTime.Now;
            db.Mantises.Add(mantises);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = mantises.ID }, mantises);
        }

      
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MantisesExists(int id)
        {
            return db.Mantises.Count(e => e.ID == id) > 0;
        }
    }
}