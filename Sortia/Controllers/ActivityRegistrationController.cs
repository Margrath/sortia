﻿using System;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using Sortia.Models;

namespace Sortia.Controllers
{
    public class ActivityRegistrationController : ApiController
    {
        private SortiaEntities db = new SortiaEntities();

        // GET: api/ActivityRegistration
        [ResponseType(typeof(ActivityRegistration))]
        public IHttpActionResult GetActivityRegistration()
        {
            var re = Request;
            var headers = re.Headers;
            if (headers.Contains("Token"))
            {
                string token = headers.GetValues("Token").First();
                bool is_valid = Helpers.TokenValidation.ValidateToken(token);
                if (is_valid)
                {
                    if (headers.Contains("ID_Employees"))
                    {
                        int ID = Convert.ToInt32(headers.GetValues("ID_Employees").First());
                        var Ar = (from act in db.ActivityRegistration where act.ID_Employees == ID select act);
                        return Ok(Ar);
                    }
                    return Ok(db.ActivityRegistration);
                }
                return Content(HttpStatusCode.BadRequest, "Invalid Token.");
            }

            return Content(HttpStatusCode.BadRequest, "Bad Request! Token missing.");
        }


        // PUT: api/ActivityRegistration/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutActivityRegistration(int id, ActivityRegistration activityRegistration)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != activityRegistration.ID)
            {
                return BadRequest();
            }

            var re = Request;
            var headers = re.Headers;

            if (headers.Contains("Token"))
            {
                string token = headers.GetValues("Token").First();
                bool is_valid = Helpers.TokenValidation.ValidateToken(token);
                if (is_valid)
                {
                        db.Entry(activityRegistration).State = System.Data.Entity.EntityState.Modified;
                        try
                        {
                            db.SaveChanges();
                        }
                        catch (DbUpdateConcurrencyException)
                        {
                            if (!ActivityRegistrationExists(id))
                            {
                                return NotFound();
                            }
                            else
                            {
                                throw;
                            }
                        }
                        return StatusCode(HttpStatusCode.NoContent);     
                }
                return Content(HttpStatusCode.BadRequest, "Invalid Token.");
            }
            return Content(HttpStatusCode.BadRequest, "Bad Request! Token missing.");

        }

        // POST: api/ActivityRegistration
        [ResponseType(typeof(ActivityRegistration))]
        public IHttpActionResult PostActivityRegistration(ActivityRegistration activityRegistration)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ActivityRegistration.Add(activityRegistration);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = activityRegistration.ID }, activityRegistration);
        }

        // DELETE: api/ActivityRegistration/5
        [ResponseType(typeof(ActivityRegistration))]
        public IHttpActionResult DeleteActivityRegistration(int id)
        {
            ActivityRegistration activityRegistration = db.ActivityRegistration.Find(id);
            if (activityRegistration == null)
            {
                return NotFound();
            }
            var re = Request;
            var headers = re.Headers;

            if (headers.Contains("Token"))
            {
                string token = headers.GetValues("Token").First();
                bool is_valid = Helpers.TokenValidation.ValidateToken(token);
                if (is_valid)
                {
                    db.ActivityRegistration.Remove(activityRegistration);
                    db.SaveChanges();

                    return Ok(activityRegistration);
                }
                return Content(HttpStatusCode.BadRequest, "Invalid Token.");
            }
            return Content(HttpStatusCode.BadRequest, "Bad Request! Token missing.");

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ActivityRegistrationExists(int id)
        {
            return db.ActivityRegistration.Count(e => e.ID == id) > 0;
        }
    }
}