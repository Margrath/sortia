﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Sortia.Models;
using Sortia.Helpers;

namespace Sortia.Controllers
{
    public class EmployeesController : ApiController
    {
        private SortiaEntities db = new SortiaEntities();

        // GET: api/Employees
        public IQueryable<Employees> GetEmployees()
        {
            return db.Employees;
        }

        // POST: api/Employees
        [ResponseType(typeof(Employees))]
        public IHttpActionResult PostEmployees(Employees employees)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            string pass=SaltPassword.getSalted(employees.Password);
            employees.Password = pass;
            employees.DateCreated = DateTime.Now;
            db.Employees.Add(employees);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = employees.ID }, employees);
        }

       

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EmployeesExists(int id)
        {
            return db.Employees.Count(e => e.ID == id) > 0;
        }
    }
}