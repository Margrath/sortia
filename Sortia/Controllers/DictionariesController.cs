﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Sortia.Helpers;

namespace Sortia.Controllers
{
    public class DictionariesController : ApiController
    {
        // GET: api/Dictionaries/{dictName}
        public IHttpActionResult GetDictionary(string dictName)
        {
            if (dictName == null || dictName=="" || dictName == "{}")
            {
                return Content(HttpStatusCode.BadRequest, "Bad Request! You have to pass a dictionary name.");
            }

            string checkdict= Dictionaries.CheckDict(dictName);
            if (checkdict!="True")
            {
                return Content(HttpStatusCode.BadRequest, checkdict);
            }
            object jsonDict;
            try {
                 jsonDict = Dictionaries.GetDict(dictName);
            }
            catch
            {
                return Content(HttpStatusCode.BadRequest, "Unexpected problems with connecting to database.");
            }

            return Ok(jsonDict);
        }

    }
}
